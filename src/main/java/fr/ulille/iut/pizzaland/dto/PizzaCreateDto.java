package fr.ulille.iut.pizzaland.dto;

import java.util.List;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaCreateDto {
	private List<Ingredient> ingredients;

	
	public PizzaCreateDto() {
		
	}
	
	public List<Ingredient> getIngredients() {
		return this.ingredients;
	}
	
	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
}
