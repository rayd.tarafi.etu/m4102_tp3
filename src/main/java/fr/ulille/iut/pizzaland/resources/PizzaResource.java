package fr.ulille.iut.pizzaland.resources;

import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

@Path("/pizza")
public class PizzaResource {
	private static final Logger LOGGER = Logger.getLogger(IngredientResource.class.getName());
	private PizzaDao pizza;
	
	public PizzaResource() {
		pizza = BDDFactory.buildDao(PizzaDao.class);
		pizza.createTables();
	}
	
	@GET
	public List<PizzaDao> getAll(){
		LOGGER.info("PizzaResource:getAll");
		
		List<PizzaDto> l = pizza.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
		LOGGER.info(l.toString());
		return l;
	}
	
	
	@GET
	@Path("{id}")
	public Pizza getPizza(UUID id) {
		return this.pizza.getPizzaById(id);
	}
	
}
