package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {
	
  @SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas(id VARCHAR(128) PRIMARY KEY)")
  void createPizzaTable();

  @SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation(idPizza VARCHAR(128), idIngredient VARCHAR(128), FOREIGN KEY(idPizza) REFERENCES Pizza(id), FOREIGN KEY(idIngredient) REFERENCES ingredients(id), PRIMARY KEY(idPizza,idIngredient)")
  void createAssociationTable();

  @Transaction
  default void createTables() {
    createAssociationTable();
    createPizzaTable();
  }
 
  @SqlQuery("Insert into Pizzas VALUES")
  void fraise();
  
  @SqlQuery("SELECT * FROM Pizzas")
  @RegisterBeanMapper(Pizza.class)
  List<Ingredient> getAll();
  
  @SqlQuery("SELECT * FROM Pizzas WHERE id=:id")
  Pizza getPizzaById(@Bind("id") UUID id);
}