package fr.ulille.iut.pizzaland.beans;

import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	
	private UUID id = UUID.randomUUID();
	private List<Ingredient> ingredients;
	
	public Pizza(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	
	public Pizza(List<Ingredient> ingredients, UUID id) {
		this.ingredients = ingredients;
		this.id = id;
	}
	
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public List<Ingredient> getIngredients() {
		return ingredients;
	}
	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public static PizzaDto toDto(Pizza p) {
    	PizzaDto dto = new PizzaDto();
    	dto.setId(p.getId());
    	dto.setIngredients(p.getIngredients());
    	return dto;
    }

    public static Pizza fromDto(PizzaDto dto) {
    	Pizza pizza = new Pizza(dto.getIngredients());
    	pizza.setId(dto.getId());
    	return pizza;
    }

    public static PizzaCreateDto toCreateDto(Pizza pizza) {
    	PizzaCreateDto dto = new PizzaCreateDto();
    	dto.setIngredients(pizza.getIngredients());
    	return dto;
    }
    
    public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
    	Pizza pizza = new Pizza(dto.getIngredients());
    	pizza.setIngredients(dto.getIngredients());
    	return pizza;
    }
    
    @Override
    public String toString() {
    	return "Pizza  [id="+ id +", ingredients=" + ingredients.toString() + "]";
    }
	
}

